package org.example.clock;

import org.junit.Before;
import org.junit.Test;

import java.time.Clock;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ClassWithTimeOperationsTest {
    private ClassWithTimeOperations testedInstance;

    @Before
    public void setUp() {
        Clock fakeClock = Clock.fixed(OffsetDateTime.of(2020,11, 8, 11, 0, 0, 0, ZoneOffset.UTC).toInstant(), ZoneId.of("UTC"));
        testedInstance = new ClassWithTimeOperations(fakeClock);
    }

    @Test
    public void shouldReturnFalseWhenTimeIsBefore() {
        OffsetDateTime parameter = OffsetDateTime.of(2020, 11, 8, 10, 0, 0, 0, ZoneOffset.UTC);

        assertFalse(testedInstance.validateDate(parameter));
    }

    @Test
    public void shouldReturnFalseWhenTimeIsEquals() {
        OffsetDateTime parameter = OffsetDateTime.of(2020, 11, 8, 11, 0, 0, 0, ZoneOffset.UTC);

        assertFalse(testedInstance.validateDate(parameter));
    }

    @Test
    public void shouldReturnTrueWhenTimeIsAfter() {
        OffsetDateTime parameter = OffsetDateTime.of(2020, 11, 8, 12, 0, 0, 0, ZoneOffset.UTC);

        assertTrue(testedInstance.validateDate(parameter));
    }
}
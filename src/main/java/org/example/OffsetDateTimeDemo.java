package org.example;

import java.time.OffsetDateTime;
import java.time.OffsetTime;

public class OffsetDateTimeDemo {
    public static void main(String[] args) {
        OffsetTime offsetTime = OffsetTime.now();
        System.out.println(offsetTime.getOffset());

        System.out.println(offsetTime);

        OffsetDateTime offsetDateTime = OffsetDateTime.now();

        System.out.println(offsetDateTime);
    }
}

package org.example;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class LocalTimeDemo {
    public static void main(String[] args) {
        LocalTime now = LocalTime.now();
        LocalTime time = LocalTime.of(11, 0);

        System.out.println(now);
        System.out.println(time);

        time.getHour();
        time.getMinute();

        System.out.println(time.isAfter(now));
        System.out.println(time.isBefore(now));

        System.out.println(now.truncatedTo(ChronoUnit.HOURS));
    }
}

package org.example;

import java.time.*;
import java.time.chrono.Chronology;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalQueries;
import java.time.temporal.TemporalQuery;
import java.util.List;

public class TemporalQueryDemo {
    public static void main(String[] args) {
//        customQueryExample();
        pluralQueriesExample();
    }

    private static void customQueryExample() {
        TemporalQuery<Boolean> holidayQuery = temporal -> {
            int dayOfWeek = temporal.get(ChronoField.DAY_OF_WEEK);
            List<MonthDay> nationalHolidays = List.of(
                    MonthDay.of(1, 1),
                    MonthDay.of(1, 7),
                    MonthDay.of(3, 8),
                    MonthDay.of(6, 28),
                    MonthDay.of(8, 24)
            );
            return dayOfWeek == DayOfWeek.SATURDAY.getValue()
                    || dayOfWeek == DayOfWeek.SUNDAY.getValue()
                    || nationalHolidays.contains(MonthDay.from(temporal)) ;
        };

        System.out.println("Query for Sunday: " + holidayQuery.queryFrom(LocalDate.of(2020, 11, 15)));
        System.out.println("Query for Monday: " + holidayQuery.queryFrom(LocalDate.of(2020, 11, 16)));
        System.out.println("Query for Women Day: " + holidayQuery.queryFrom(LocalDate.of(2020, 3, 8)));
    }

    private static void pluralQueriesExample() {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Amsterdam"));


        LocalDate date = now.query(TemporalQueries.localDate());
        LocalTime time = now.query(TemporalQueries.localTime());
        Chronology chronology = now.query(TemporalQueries.chronology());
        ZoneOffset zoneOffset = now.query(TemporalQueries.offset());
        ZoneId zoneId = now.query(TemporalQueries.zoneId());

        System.out.println("Date: " + date);
        System.out.println("Time: " + time);
        System.out.println("Chronology: " + chronology);
        System.out.println("Zone Offset: " + zoneOffset);
        System.out.println("Zone Id: " + zoneId);
    }
}

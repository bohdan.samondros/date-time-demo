package org.example;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class InstantDemo {
    public static void main(String[] args) {
        Instant now = Instant.now();

        System.out.println(now);

        System.out.println(Instant.MIN);
        System.out.println(Instant.ofEpochMilli(0L));
        System.out.println(Instant.MAX);



        ZonedDateTime utc = ZonedDateTime.of(1950, 1, 1, 0, 0, 0, 0, ZoneId.of("UTC"));
        System.out.println(utc.toInstant().getEpochSecond());
    }
}

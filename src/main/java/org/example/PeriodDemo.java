package org.example;

import java.time.LocalDate;
import java.time.Period;

public class PeriodDemo {
    public static void main(String[] args) {
        LocalDate date1 = LocalDate.of(2010, 8, 10);
        LocalDate date2 = LocalDate.of(2020, 4, 26);

        Period period = Period.between(date1, date2);

        System.out.println("Period years: " + period.getYears());
        System.out.println("Period months: " + period.getMonths());
        System.out.println("Period days: " + period.getDays());
        System.out.println(period);

        Period p = Period.parse("P4W");
        System.out.println(p);
    }
}

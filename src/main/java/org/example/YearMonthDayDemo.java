package org.example;

import java.time.*;
import java.util.Date;

public class YearMonthDayDemo {
    public static void main(String[] args) {
        Year nowYear = Year.now();
        System.out.println(nowYear);

        Date now = new Date(ZonedDateTime.now().toInstant().toEpochMilli());

        YearMonth nowYearMonth = YearMonth.now();
        System.out.println(nowYearMonth);

        MonthDay nowMonthDay = MonthDay.now();

        System.out.println(nowMonthDay);
    }
}

package org.example;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ZonedDateTimeDemo {
    public static void main(String[] args) {
        ZonedDateTime now = ZonedDateTime.now();
        System.out.println(now);

        ZonedDateTime sameTimeInAustralia = now.withZoneSameInstant(ZoneId.of("Australia/Perth"));
        System.out.println(sameTimeInAustralia);

        ZonedDateTime sameValueButInAustralia = now.withZoneSameLocal(ZoneId.of("Australia/Perth"));
        System.out.println(sameValueButInAustralia);

        ZonedDateTime sameValueButInAustralia2 = now.withZoneSameLocal(ZoneId.of("UTC+2"));
        System.out.println(sameValueButInAustralia2);
    }
}

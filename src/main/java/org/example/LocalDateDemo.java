package org.example;

import java.time.LocalDate;
import java.time.temporal.ChronoField;

public class LocalDateDemo {
    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        LocalDate time = LocalDate.now();

        int year = today.get(ChronoField.YEAR);

        System.out.println(year);
        System.out.printf("Является ли год %s высокосным? %s%n", year, today.isLeapYear());
    }
}

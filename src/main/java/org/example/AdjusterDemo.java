package org.example;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

public class AdjusterDemo {
    public static void main(String[] args) {
//        example1();
//        example2();
//        example3();
        example4();
    }


    // получить следующая ближайшая суббота
    public static void example1() {
        LocalDate now = LocalDate.now();

        LocalDate nearestSunday = now.with(TemporalAdjusters.firstInMonth(DayOfWeek.TUESDAY));
        System.out.println(nearestSunday);
    }

    // получить первй день текущего месяца
    public static void example2() {
        LocalDate now = LocalDate.now();

        LocalDate nearestSunday = now.with(TemporalAdjusters.firstDayOfMonth());
        System.out.println(nearestSunday.getDayOfWeek());
    }

    // получить первй день текущего месяца
    public static void example3() {
        LocalDate now = LocalDate.now();

        TemporalAdjuster adjuster = temporal -> temporal.plus(14, ChronoUnit.DAYS);
        LocalDate adjustedDate = now.with(adjuster);
        System.out.println(adjustedDate);
    }

    public static void example4() {
        LocalDate now = LocalDate.of(2020, 11, 14);
//        LocalDate now = LocalDate.now();

        LocalDate adjustedDate = now.with(NEXT_WORKING_DAY);
        System.out.println(adjustedDate);
    }


    static TemporalAdjuster NEXT_WORKING_DAY = TemporalAdjusters.ofDateAdjuster(date -> {
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        int daysToAdd;
        if (dayOfWeek == DayOfWeek.FRIDAY)
            daysToAdd = 3;
        else if (dayOfWeek == DayOfWeek.SATURDAY)
            daysToAdd = 2;
        else
            daysToAdd = 1;
        return date.plusDays(daysToAdd);
    });
}

package org.example.clock;

import java.time.Clock;
import java.time.OffsetDateTime;

public class ClassWithTimeOperations {
    private final Clock clock;

    public ClassWithTimeOperations() {
        this.clock = Clock.systemDefaultZone();
    }

    ClassWithTimeOperations(Clock clock) {
        this.clock = clock;
    }

    public boolean validateDate(OffsetDateTime dateTime) {
        if (dateTime.isAfter(OffsetDateTime.now(clock))) {
            return true;
        } else {
            return false;
        }
    }
}

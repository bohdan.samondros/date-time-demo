package org.example.clock;

import java.time.ZoneId;

public class Main {
    public static void main(String[] args) {
        ZoneId.getAvailableZoneIds().forEach(it -> {
            System.out.println(it);
        });
    }
}

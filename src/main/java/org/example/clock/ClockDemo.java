package org.example.clock;

import java.time.*;

public class ClockDemo {
    public static void main(String[] args) {
        Clock utcClock = Clock.systemUTC();
        System.out.println(utcClock);
        System.out.println(utcClock.instant());

        Clock systemClock = Clock.systemDefaultZone();
        System.out.println(systemClock);

        Clock systemByZone = Clock.system(ZoneId.of("Australia/Perth"));
        System.out.println(systemByZone);

        Clock fixedClock = Clock.fixed(OffsetDateTime.of(2020, 11, 8, 11, 0, 0, 0, ZoneOffset.UTC).toInstant(), ZoneId.of("UTC"));

        LocalDateTime localTime = LocalDateTime.now(fixedClock);
        System.out.println(localTime);

        OffsetDateTime offsetDateTime = OffsetDateTime.now(fixedClock);
        System.out.println(offsetDateTime);

        ZonedDateTime zonedDateTime = ZonedDateTime.now(fixedClock);
        System.out.println(zonedDateTime);
    }
}

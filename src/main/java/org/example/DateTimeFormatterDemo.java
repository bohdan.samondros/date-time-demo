package org.example;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.chrono.Chronology;
import java.time.chrono.ThaiBuddhistChronology;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoField;
import java.util.Locale;

public class DateTimeFormatterDemo {
    public static void main(String[] args) {
//        example1();
//        builderExample();
        example3();
    }

    private static void example1() {
        ZonedDateTime now = ZonedDateTime.now();

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd HH:mm:ss,SSSSSSzZ");

        System.out.println(dateTimeFormatter.format(now));
        System.out.println(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(now));
        System.out.println(DateTimeFormatter.ISO_ZONED_DATE_TIME.format(now));
        System.out.println(DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(now));

        DateTimeFormatter dateTimeFormatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
        System.out.println(dateTimeFormatter2.format(now));

        LocalDateTime parsedLocalDateTime = LocalDateTime.parse("2020-11-07 09:40:53 PM", dateTimeFormatter2);
        System.out.println(parsedLocalDateTime);
    }

    public static void builderExample() {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder().
                parseCaseInsensitive()
                .optionalStart()
                .appendText(ChronoField.YEAR)
                .appendLiteral('-')
                .appendValue(ChronoField.MONTH_OF_YEAR)
                .appendLiteral('-')
                .appendText(ChronoField.DAY_OF_MONTH)
                .optionalStart()
                .appendLiteral(' ')
                .optionalEnd()
                .optionalEnd()
                .optionalStart()
                .appendValue(ChronoField.HOUR_OF_DAY, 2)
                .appendLiteral(':')
                .appendValue(ChronoField.MINUTE_OF_HOUR, 2)
                .appendLiteral(':')
                .appendValue(ChronoField.SECOND_OF_MINUTE, 2)
                .optionalEnd()
                .optionalStart()
                .appendOffsetId()
                .optionalEnd()
                .toFormatter();

        System.out.println("LocalDateTime: " + LocalDateTime.now().format(formatter));
        System.out.println("LocalDate: " + LocalDate.now().format(formatter));
        System.out.println("LocalTime: " + LocalTime.now().format(formatter));

        System.out.println(formatter.parse("2020-11-10 21:37:03Z"));
        System.out.println(formatter.parse("2020-11-10Z"));
        System.out.println(formatter.parse("21:37:03Z"));
    }

    public static void example3() {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL)
//                .withLocale()
                .withChronology(ThaiBuddhistChronology.INSTANCE);

        System.out.println(formatter.format(ZonedDateTime.now()));
    }
}

package org.example;

import java.time.*;

public class DurationDemo {
    public static void main(String[] args) {
        LocalTime time1 = LocalTime.of(4, 10);
        LocalTime time2 = LocalTime.of(8, 26, 10, 67667);

//        ZonedDateTime nowZoned = ZonedDateTime.of(2020, 10, 1 , 0, 0, 0, 0,ZoneId.of("UTC"));
//        ZonedDateTime amsterdamZoned = ZonedDateTime.now(ZoneId.of("Europe/Amsterdam"));

//        Duration duration = Duration.between(time1, time2);
        Duration duration = Duration.between(time1, time2);

        System.out.println("Duration seconds: " + duration.getSeconds());
        System.out.println("Duration nanos: " + duration.getNano());
        System.out.println(duration);
    }
}
